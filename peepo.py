import discord
import logging
import os
import random
import subprocess
import traceback
import praw

from shutil import which
from discord.ext import commands
from configparser import ConfigParser

logging.basicConfig(level=logging.INFO)


def run_command(cmd_list):
    stdout = subprocess.PIPE
    stderr = open(os.devnull, "w")
    process = subprocess.run(cmd_list, stdout=stdout, stderr=stderr)
    return process.stdout.decode("utf-8")


class Peepo(commands.Bot):
    def __init__(self, config):
        super().__init__(command_prefix=config.get("setup", "prefix"))
        self.config = config

    async def on_ready(self):
        logging.log(logging.INFO, "Successfully logged in")

        status = discord.Status.idle
        activity = discord.Game(name=self.config.get("status", "active"))
        await self.change_presence(status=status, activity=activity)

    async def on_message(self, message):
        if message.author.bot:
            return

        # Logging the command - currently useless
        # cmd, *args = re.split(r"\s+", message.content)
        # logging.log(logging.INFO, "Message from %s:\nCommand: %s\nArgs: %s", message.author, cmd, args)

        await self.process_commands(message)

    async def on_error(self, event_method, *args, **kwargs):
        pass

    async def on_command_error(self, ctx, e):
        e_str = "".join(traceback.format_exception(type(e), e, e.__traceback__))
        logging.log(logging.ERROR, "Exception caught:\n%s", e_str)
        await ctx.send(e)


class General(commands.Cog):
    def __init__(self, bot, reddit):
        self.bot = bot
        self.reddit = reddit

    @commands.command()
    async def say(self, ctx, *args):
        await ctx.send(" ".join(args))

    @commands.command()
    async def roll(self, ctx, *args):
        try:
            author = ctx.message.author
            low = 0 if len(args) <= 1 else int(args[0])
            high = 100 if len(args) == 0 else int(args[-1])
            rand_num = random.randint(low, high)

            await ctx.send("{} has rolled a {}".format(author, rand_num))
        except ValueError as e:
            await ctx.send("[roll] Invalid arguments: {}".format(args))

    @commands.command()
    async def repo(self, ctx, *args):
        await ctx.send("https://www.gitlab.com/julian-heng/peepo")

    @commands.command()
    async def sort(self, ctx, *args):
        if len(args) == 0:
            await ctx.send("_ _")
            return

        args = list(args)
        args.sort()
        await ctx.send("\n".join(args))

    @commands.command()
    async def crash(self, ctx, *args):
        raise Exception("Crashed!!!")

    @commands.command()
    async def fortune(self, ctx, *args):
        if which("fortune") is None:
            raise Exception("Missing fortune program")

        await ctx.send("Peepo says:\n>>> {}".format(run_command(["fortune"])))

    @commands.command()
    async def sponge(self, ctx, *args):
        if len(args) == 0:
            await ctx.send("_ _")
            return

        yes_or_no = lambda: random.randint(1, 2) % 2 == 0
        rand_case = lambda i: i if yes_or_no() else i.swapcase()
        spongecase = lambda s: "".join([rand_case(i) for i in s])

        out = " ".join([spongecase(i) for i in args])

        await ctx.send("{}".format(out))

    @commands.command()
    async def reddit(self, ctx, *args):
        err_fmt = "{{}}: {}reddit [subreddit] [hot|top] {{{{limit}}}}"
        err_fmt = err_fmt.format(self.bot.command_prefix)

        if len(args) == 0 or len(args) == 1:
            await ctx.send(err_fmt.format("Not enough arguments"))
            return

        sub, mode, limit = (list(args) + ([None] * 1))[:3]

        sub = self.reddit.subreddit(sub)
        limit = 3 if limit is None else int(limit)

        modes = {
            "hot": sub.hot,
            "top": sub.top
        }

        if mode not in modes:
            await ctx.send(err_fmt.format("Invalid mode"))
            return

        fmt_out = lambda n, i: "{}. {}".format(n, i.title)
        fetch = lambda m: enumerate(modes[m](limit=limit))

        out = "\n".join([fmt_out(n + 1, i) for (n, i) in fetch(mode)])
        await ctx.send(out)

    @commands.command()
    async def flip(self, ctx, *args):
        coin_flip = ['You flipped head!', 'You flipped tail!']
        await ctx.send("{}".format(random.choice(coin_flip)))


def main():
    config = ConfigParser()
    config.read("settings.ini")

    token = config.get("setup", "token")
    reddit = praw.Reddit(client_id=config.get("reddit", "id"),
                         client_secret=config.get("reddit", "secret"),
                         user_agent=config.get("reddit", "agent"))

    bot = Peepo(config)
    bot.add_cog(General(bot, reddit))
    bot.run(token)


if __name__ == "__main__":
    main()
